# GeckoEdge

GeckoEdge 是一个Java 8语言编写的，基于事件驱动的设备管理框架，可以应用于门禁系统、智能家居、停车场系统等硬件控制管理领域。

GeckoEdge 是基于我以前写的多个开源项目
[AthenaNode](https://gitee.com/yoojia/AthenaNode)、
[NextCherry](https://gitee.com/yoojia/NextCherry)、
[AthenaZigBee](https://gitee.com/yoojia/AthenaZigBee)
[AthenaZStack](https://gitee.com/yoojia/AthenaZStack)
的思路，重新设计和编写。它被设计成可以在各种性能设备上运行本地业务逻辑计算，提供本地硬件之间事件通讯能力的软件框架，即类似物联网边缘计算的概念。
通过虚拟硬件管道，可以实现不同硬件通讯协议、不同消息格式的的设备管理；借助于类似MQTT服务、ZeroMQ通讯网络、阿里云IOT平台等硬件通讯能力，
可以实现多节点网络协同管理的能力。

## 项目模块

本项目中已实现的模块介绍

1. [app](app/README.md): 应用程序模块，基于Jetty服务器，运行 GeckoEngine 和 Web后端功能；
1. [core-edge](core-edge/README.md): 核心模块，包含 GeckoEdge 的框架接口和流程管理；
1. [pipeline-aliiot](pipeline-aliiot/README.md): 阿里云物联网平台设备路由实现，可以接入AliIOT设备进行远程控制；
1. [pipeline-mqtt](pipeline-mqtt/README.md): MQTT v3 物联网协议设备路由实现，可以接入MQTT设备进行远程控制；
1. [pipeline-socket](pipeline-socket/README.md): Socket通讯设备，支持TCP/UDP协议通讯；
1. [support-dingtalk](support-dingtalk/README.md): 钉钉办公系统支持模块，实现员工权限校验、员工自动同步等功能；
1. [support-dongkong](support-dongkong/README.md): 东控门禁硬件模块的通讯协议；

## 系统事件通讯流程

![](GECKO-ARCH.png)

- `VirtualTrigger` 虚拟触发器： 设备状态变化、其它Web服务、云服务等事件的入口；它是触发一次设备动作的起源位置。
- `Interceptor` 拦截器：对触发器发起的事件，进行拦截判断处理。
- `Driver` 用户驱动：对触发器事件执行业务相关计算，从而驱动硬件执行动作；
- `DevicePipeline` 设备管道：它负责建立具体设备与系统的通讯管理，为设备与系统之间提供通讯环境；
- `VirtualDevice` 虚拟设备：最终执行设备动作的抽象表示，它可以是执行RS485、串口、GPIO/PWN等硬件通讯协议驱动，
    也可以是使用TCP、MQTT、阿里云IOT等网络通讯的虚拟设备，最终由远程真实设备执行。

## VirtualTrigger 虚拟触发器

设备状态变化、其它Web服务、云服务等事件的入口；它是触发一次设备动作的起源位置。

详见：[虚拟触发器](TRIGGER.md)

## Interceptor - 拦截器

Interceptor是事件拦截器，负责对触发器发起的事件进行拦截处理，不符合规则的事件将被中断，丢弃。

详见：[拦截器](INTERCEPTOR.md)

## Driver - 用户驱动

Driver是实现设备与设备之间联动，关联的核心接口。
当设备事件通过拦截器后，交由Driver来处理事件，在具体实现类中，Driver负责实现核心业务逻辑。

详见：[用户驱动](DRIVER.md)

## DevicePipeline - 设备管道

DevicePipeline 负责为某一通讯协议

在默认实现中，DevicePipeline 提供多种通讯协议接入方式：

1. [GPIO、串口设备集群](ONBOARD_ROUTER.md)
1. [RS485协议设备集群](RS485_ROUTER.md)
2. [TCP协议设备集群](TCP_ROUTER.md)
4. [MQTT协议设备集群](router-mqtt/README.md)
4. [阿里云IOT物联网平台](router-aliiot/README.md)

## VirtualDevice - 虚拟硬件设备

VirtualDevice是具体硬件对象的表示类，实现`请求-响应`的控制指令。

详见：[虚拟硬件](VIRTUAL_DEVICE.md)

## 其它说明文档

1. [设备地址定义规则](DEVICE_ADDRESS.md)
1. [事件TOPIC规则](EVENT_TOPIC.md)
1. [SPI 组件配置](SPI_CONFIG.md)

## 支持其它平台

1. 阿里云 IOT 平台：[阿里云物联网套件](https://iot.aliyun.com/docs/suite/introduction/architecture.html)
1. 钉钉开放平台： [钉钉开放平台](https://open-doc.dingtalk.com/)
