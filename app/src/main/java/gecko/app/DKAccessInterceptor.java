package gecko.app;

import gecko.AbstractInterceptor;
import gecko.GeckoContext;
import gecko.GeckoScoped;

/**
 * 东控门禁刷卡拦截器
 *
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public class DKAccessInterceptor extends AbstractInterceptor {

    @Override
    public void handle(GeckoContext context, GeckoScoped scoped) throws Exception {

        System.out.println(context.inbound().data().toJSONString());
    }
}
