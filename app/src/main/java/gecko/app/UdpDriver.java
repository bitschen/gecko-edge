package gecko.app;

import gecko.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeoutException;

/**
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public class UdpDriver extends AbstractDriver {

    private static final Logger LOGGER = LoggerFactory.getLogger(UdpDriver.class);

    @Override
    public void handle(GeckoContext context, PipelineSelector selector, GeckoScoped scoped) throws Exception {
        final DevicePipeline pipeline = selector.find("udp");
        pipeline.getByGroupAddress("127.0.0.1").forEach(dev -> {
            final EventFrame req = EventFrame.create(context.contextId(), "PING".getBytes());
            try {
                System.out.println(dev.process(req, scoped).get());
            } catch (TimeoutException to) {
                to.printStackTrace();
            } catch (Exception ex) {
                LOGGER.error("设备错误", ex);
            }
        });
    }
}

