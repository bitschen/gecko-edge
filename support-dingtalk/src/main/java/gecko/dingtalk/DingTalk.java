package gecko.dingtalk;

/**
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public interface DingTalk {

    String CONF_ACCESS_TOKEN = "dingTalkAccessToken";
}
