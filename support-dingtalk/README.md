# 第三方支持功能 - 钉钉

1. 支持钉钉系统同步企业通讯录功能；员工入职离职自动管理；
1. 支持钉钉系统用户权限验证，可校验用户是否属于钉钉系统；
1. 支持钉钉系统机器人上报：系统故障；
1. 支持钉钉系统机器人上报：非工作时间开门通知；

## 钉钉系统配置

需要在**\[GLOBALS]**配置块中，设置钉钉系统的AccessToken；

```toml
[GLOBALS]
 dingTalkAccessToken = "xxxxxx"
```

## 钉钉系统设备故障上报机器人

通过钉钉故障上报机器人，可以实时才将设备故障消息发送到钉钉群中。

![ROBOT](DT_ERR_REPORT_ROBOT.png)

```toml
[INTERCEPTORS.SPI.DingTalkErrorReportRobot]
 disable = false
 topics = [ "/dongkong/errors/#"]
[INTERCEPTORS.SPI.DingTalkErrorReportRobot.initArgs]
 robotToken = "56d8d2255b81cba7db7dd576209c549e6f0273bd17b562c89938f3dbab57144a"
 fieldTime = "时间"   
 fieldSN = "序列"   
 fieldAddr = "地址"   
 fieldReason = "原因"   
```

其中：

- topics 指定监听内部哪一类型的事件Topic；
- robotToken 钉钉群机器人的Token，从钉钉软件群中添加并获取；
- fieldTime 机器人上报的消息的时间字段，默认为“时间”；
- fieldSN 机器人上报的消息的序列字段，默认为“序列”；
- fieldAddr 机器人上报的消息的地址字段，默认为“地址”；
- fieldReason 机器人上报的消息的原因字段，默认为“原因”；

## 钉钉系统用户权限校验

钉钉用户授权拦截器，可以对Interceptor阶段的用户信息进行拦截。它需要与其它模块共同使用：

1. 在 Context.attributes 中指定 `auth-type = dingtalk`；
1. 在 Context.attributes 中指定 `userid = xxxx`；

### 配置信息

配置需要指定 `priority` 优先级要低于辅助模块；

```toml
[INTERCEPTORS.SPI.DingTalkAuthInterceptor]
 disable = false
 priority = 100
 topics = [
  "/auth/#"
 ]

```

## 使用

