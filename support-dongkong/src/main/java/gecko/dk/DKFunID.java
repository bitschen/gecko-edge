package gecko.dk;

/**
 * 东控门禁协议功能ID
 *
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public interface DKFunID {

    /**
     * 搜索控制主板
     */
    byte SEARCH_BOARD = (byte) 0x94;

    /**
     * 设置控制板地址
     */
    byte SET_BOARD_ADDR = (byte) 0x96;

    /**
     * 控制板状态信息
     */
    byte BOARD_STATE = (byte) 0x20;

    /**
     * 设置控制板时间
     */
    byte WRITE_REMOTE_TIME = (byte) 0x30;

    /**
     * 读取控制板时间
     */
    byte READ_REMOTE_TIME = (byte) 0x32;

    /**
     * 远程开闸
     */
    byte REMOTE_OPEN = (byte) 0x32;
}
