package gecko.dk;

import java.nio.ByteBuffer;

/**
 * 设置设备IP地址指令
 *
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public class InstrBoardAddress extends Instr {

    /**
     * 创建指令
     *
     * @param devSN     设备序列号
     * @param devIP     设备IP
     * @param gatewayIP 网关IP
     * @param maskIP    子网掩码
     */
    public InstrBoardAddress(int devSN, String devIP, String gatewayIP, String maskIP) {
        super(
                (byte) 0x96,
                devSN,
                0,
                data(devIP, gatewayIP, maskIP)
        );
    }

    /**
     * 创建指令；子网掩码默认为 255.255.255.0
     *
     * @param devSN     设备序列号
     * @param devIP     设备IP
     * @param gatewayIP 网关IP
     */
    public InstrBoardAddress(int devSN, String devIP, String gatewayIP) {
        this(devSN, devIP, gatewayIP, "255.255.255.0");
    }

    public static InstrBoardAddress create(int devSN, String devIP, String gatewayIP) {
        return new InstrBoardAddress(devSN, devIP, gatewayIP);
    }

    public static InstrBoardAddress create(int devSN, String devIP, String gatewayIP, String maskIP) {
        return new InstrBoardAddress(devSN, devIP, gatewayIP, maskIP);
    }

    private static byte[] data(String devIP, String gatewayIP, String maskIP) {
        final ByteBuffer data = makeBuffer(DATA_SIZE);
        data.put(ip2Bytes(devIP));
        data.put(ip2Bytes(maskIP));
        data.put(ip2Bytes(gatewayIP));
        data.put((byte) 0x55);
        data.put((byte) 0xAA);
        data.put((byte) 0xAA);
        data.put((byte) 0x55);
        data.putInt(0); // fill zero
        return data.array();
    }
}
