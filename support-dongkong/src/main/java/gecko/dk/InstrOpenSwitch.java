package gecko.dk;

/**
 * 远程开门指令
 *
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public class InstrOpenSwitch extends Instr {

    /**
     * 创建指令
     *
     * @param devSN    设备序列号
     * @param switchId 开头编号
     */
    public InstrOpenSwitch(int devSN, int switchId) {
        super(
                (byte) 0x40,
                devSN,
                0,
                data(switchId)
        );
    }

    public static InstrOpenSwitch create(int devSN, int switchId) {
        return new InstrOpenSwitch(devSN, switchId);
    }

    private static byte[] data(int switchId) {
        if (switchId < 1 || switchId > 4) {
            throw new IllegalArgumentException("DoorSwitchId错误: " + switchId);
        }
        final byte[] data = new byte[20];
        data[0] = (byte) switchId;
        return data;
    }
}
