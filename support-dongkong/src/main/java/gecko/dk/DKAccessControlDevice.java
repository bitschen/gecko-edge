package gecko.dk;

import gecko.GeckoScoped;
import gecko.impl.UdpVirtualDevice;
import gecko.lang.TypedMap;
import gecko.x.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 东控门禁主板通讯协议的设备，此处实现以下功能：
 * - 启动时，设置门禁主板IP地址为配置地址指定的IP地址；
 * - 启动时，设置门禁主板上报服务器IP地址为当前主机的IP地址；
 * - 启动时，设置门禁主板时间为当前主机时间；
 *
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public final class DKAccessControlDevice extends UdpVirtualDevice {

    private final static Logger LOGGER = LoggerFactory.getLogger(DKAccessControlDevice.class);

    private int mBoardSN;
    private String mGatewayIP;
    private String mServerIP;
    private int mServerPort;

    @Override
    public void onInit(TypedMap initArgs, GeckoScoped scoped) {
        super.onInit(initArgs, scoped);
        mBoardSN = initArgs.getInt("boardSN", 0);
        if (mBoardSN <= 0) {
            throw new IllegalArgumentException("未设定控制板序列号 boardSN");
        }
        mGatewayIP = initArgs.getString("gatewayIP");
        Strings.requireNotEmpty(mGatewayIP, "未设定网关IP gatewayIP");
        mServerIP = initArgs.getString("serverIP");
        Strings.requireNotEmpty(mGatewayIP, "未设定服务器IP serverIP");
        mServerPort = initArgs.getInt("serverPort");
        if (mServerPort < 1024) {
            throw new IllegalArgumentException("未设置服务器端口 serverPort");
        }
    }

    @Override
    public void onStart(GeckoScoped scoped) {
        super.onStart(scoped);
        // 设置门禁主板IP地址
        LOGGER.info("启动东控门禁主板[{}]", mBoardSN);
//        try {
//            final Instr cmd = InstrBoardAddress.create(mBoardSN, getDefineUdpAddress(), mGatewayIP);
//            LOGGER.info("设置东控门禁主板[{}]网络信息，IP： {}， 网关： {}", mBoardSN, getDefineUdpAddress(), mGatewayIP);
//            udpSend(cmd.bytes(), InetAddress.getByName("255.255.255.255"), getDefineUdpPort());
//        } catch (Exception ex) {
//            LOGGER.error("设置门禁主板IP地址失败", ex);
//        }
//        try {
//            Thread.sleep(500);
//        } catch (InterruptedException ex) {
//            LOGGER.error("设置门禁主板IP地址等待期间出错", ex);
//        }
//        // 设置门禁主板服务器地址
//        try {
//            final Instr cmd = InstrReportServer.create(mBoardSN, mServerIP, (short) mServerPort, 0);
//            LOGGER.info("设置东控门禁主板[{}]上报服务器信息，IP： {}， 端口： {}", mBoardSN, mServerIP, mServerPort);
//            udpSend(cmd.bytes(), getRemoteAddress(), getDefineUdpPort());
//        } catch (Exception ex) {
//            LOGGER.error("设置门禁服务器上报IP地址失败", ex);
//        }
        // 设置门禁主板时间
        // TODO
    }

    @Override
    protected int getDefineUdpPort() {
        // 东控主板的UDP端口固定为 60000
        return 60000;
    }
}
