# 东控门禁系统协议支持

本模块实现东控门禁硬件的UDP通讯功能和协议解析。

## DKAccessBoardDevice - 门禁主板控制设备

东控门禁主板设备，可以接收系统控制指令，发送到门禁主板，并等待响应结果；

```toml
# 东控门禁主板设备；
# 接收系统控制指令，并通过UDP通讯方式发送到门禁主板来执行控制操作；
[DEVICES.DK0755ControlBoard]
 class = "gecko.dk.DKAccessControlDevice"
 groupAddress = "192.168.1.34"
 physicalAddress = "60000"
[DEVICES.DK0755ControlBoard.initArgs]
 byteBuffKB = 1
 sendBuffSizeKB = 1
 recvBuffSizeKB = 1
 soTimeoutSec = 3
 boardSN = 223177933
 gatewayIP = "192.168.1.1"
 serverIP = "192.168.1.11"
 serverPort = 5572

```

## DKAccessUdpServer - 门禁主板实时监控UDP服务端

用于接收门禁控制板实时监控广播消息；

```toml
# 东控门禁主板数据上报服务；
# 监听UDP端口，接收门禁主板上报的事件消息，转到内部系统处理；
[TRIGGERS.SPI.DKAccessMonitorServer]
 class = "gecko.dk.DKAccessMonitorServer"
[TRIGGERS.SPI.DKAccessMonitorServer.initArgs]
 byteBuffKB = 1
 sendBuffSizeKB = 1
 recvBuffSizeKB = 1
 serverIP = "0.0.0.0"
 serverPort = 5572

```

## 通讯协议

- [东控门禁主板UDP报文协议](PROTOCOL.md)
- [东控门禁主板UDP报文协议-官方文档](东控门禁系统短报文格式.pdf)