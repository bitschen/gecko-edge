package gecko.mqtt;

import gecko.impl.FutureTransporterDevice;

/**
 * 使用阿里云IOT平台进行数据通讯的虚拟设备对象，
 * 由 {@link gecko.VirtualDevice.FutureTransporter} 实现异步RPC通讯；
 *
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public final class MqttVirtualDevice extends FutureTransporterDevice {

    static final String PROTOCOL = "mqttv3";

    @Override
    public String getProtocolName() {
        return PROTOCOL;
    }
}
