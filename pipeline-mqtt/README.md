# MqttDevicePipeline 设备集群Pipeline

支持MQTT v3协议的硬件设备， 为MqttVirtualDevice提供一个多设备管理环境和MQTT网络通讯环境。

**MqttVirtualDevice**

MqttVirtualDevice是一个代表远程设备的类，它实现通过MQTT通讯网络与远程硬件设备驱动通讯。

## MQTT RPC Topic规则

MQTT本质上是一个PUB/SUB消息系统，并不适合RPC。但通过特定规则的TOPIC定义，我们可以模拟RPC通讯。

MQTT RPC TOPIC规则如下：

> / gecko / {nodeId} @ {domain} / {eventType} / {groupAddr} / {physicalAddr} / {eventId}

其中，各个字段的意义如下：

1. `gecko`固定字符串前缀；
1. `nodeId`,`domain` 服务节点信息；
1. `eventType` 设备事件类型，它有：
    - `update` 由设备主动发起的状态更新事件，由GeckoEdge系统读取此Topic。通常为设备状态变化或对控制命名做出响应。
    - `requests` RPC控制，由GeckoEdge系统发起的请求控制事件，由目标设备读取此Topic。
    - `replies` RPC控制，由目标设备发起请求响应事件，由GeckoEdge系统读取此Topic。
    
1. `groupAddr` 属组地址，通常是某一分组名称，也可以是用户名和组织名；
1. `physicalAddr` 设备地址，它与groupAddr共同决定某一设备；

1. `eventId` 事件ID。分两种情况：
    - `eventType = update`情况下，如果是由设备发起的状态更新事件，固定为`0`；如果是对 `rpc` 控制响应，则与控制eventId相同。
    - `eventType = requests | replies` 由服务端生成一个唯一事件ID，当设备对控制指令返回响应时，以相同ID返回。

例如：

服务端通过MQTT网络向远程设备（ROOM 503开关设备）发送开门指令。开头设备打开电磁锁之后，返回响应结果。
对应的Topic通讯过程如下：

基本信息：

- groupAddr: `yoojia-sz`
- physicalAddr: `room503-gpioSwitch`

1. 服务端发起开门事件：

> /gecko/tst1@irain755/requests/yoojia-sz/room503-gpioSwitch/1544149892477

```json
{
  "command": "on"
}
```

1. MQTT设备订阅TOPIC：

> /gecko/tst1@irain755/requests/yoojia-sz/room503-gpioSwitch/+

1. MQTT设备接收到事件后，打开电磁锁，并返回开门结果：

> /gecko/tst1@irain755/replies/yoojia-sz/room503-gpioSwitch/1544149892477

```json
{
  "state": "on"
}
```

1. 服务端订阅设备响应消息的Topic：

> /gecko/tst1@irain755/replies/+/+/#

1. 服务端订阅设备状态变更消息的Topic：

> /gecko/tst1@irain755/update/+/+/0
