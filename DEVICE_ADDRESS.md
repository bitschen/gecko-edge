# 设备地址格式

事件对象Event，包含事件的来源地址和目标地址。地址格式如下：

> protocol :// nodeId @ domain / boardAddress / deviceAddress # tag

其中：

## **protocol** 通讯协议名称：

表示系统应当使用何种方式来与设备通讯。
具有相同通讯协议方式的设备，注册到系统后，会自动归并到同个DeviceHub。

- `onboard` 服务内部通讯设备；
- `rs485` 使用RS485协议来通讯的设备；
- `mqtt` 使用MQTT网络来通讯的设备；
- `tcp` 使用TCP Socket连接来通讯的设备；

## **nodeId@domain** 服务节点域：

- nodeId: 节点ID，与domain一起，决定唯一节点服务的地址；
- domain: 节点服务域；

## **groupAddress** 属组地址：

属组地址，表示此设备归于于哪一组。

它与 physicalAddress 共同决定一个设备的坐标。属组地址通常用来分组，或者声明它归属于哪个组织和用户。

例如，默认情况下：GPIO设备的属组为`gpio`，串口设备的属组为`serialport`。

## **physicalAddress** 设备物理地址：

设备物理地址，它应用于VirtualDevice的实现类，使用此地址与具体硬件驱动进行通讯。
它与groupAddress共同决定一个设备的坐标。

例如，表示`内部设备`，GPIO端口，地址为`3`的设备：

> onboard://001@irainsz/gpio/3

例如，表示`内部设备`，串口地址为`tty2`的设备：

> onboard://001@irainsz/serialport/tty2

例如，表示`rs485`协议，管理主板串口`tty6`的设备，地址端口为`12`的设备：

> rs485://001@irainsz/tty6/12

例如，表示`mqtt`协议，管理设备组为`yoojia`(管理用户名/组名)，设备地址为`room503gateswitch`的地址：

> mqtt://001@irainsz/yoojia/room503gateswitch

## **tag** 标签信息：

标签信息用于通过地址来传递额外信息的字段，一般不做使用。


