package gecko.aliiot;

import gecko.impl.FutureTransporterDevice;

/**
 * 使用阿里云IOT平台进行数据通讯的虚拟设备对象，
 *
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public final class AliIOTVirtualDevice extends FutureTransporterDevice {

    static final String PROTOCOL = "aliiot";

    @Override
    public String getProtocolName() {
        return PROTOCOL;
    }
}
