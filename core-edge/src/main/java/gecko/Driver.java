package gecko;

/**
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public interface Driver extends Bundle, TopicFilter {

    /**
     * 处理外部请求，返回响应结果。
     * 在Driver内部，可以通过 {@link PipelineSelector} 来获取需要的设备管理器，从而控制设备。
     *
     * @param ctx    GeckoContext
     * @param scoped GeckoScoped
     * @throws Exception 抛出异常
     */
    void handle(GeckoContext ctx, PipelineSelector sel, GeckoScoped scoped) throws Exception;
}
