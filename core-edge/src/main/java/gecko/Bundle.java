package gecko;

/**
 * Bundle 是一个融合了初始化，生命周期管理的接口
 *
 * @author 陈哈哈 (yoojiachen@gmail.com, chenyongjia@parkingwang.com)
 * @version 0.1
 */
interface Bundle extends InitAware {

    /**
     * 启动
     */
    default void onStart(GeckoScoped scoped) {

    }

    /**
     * 停止
     */
    default void onStop(GeckoScoped scoped) {

    }
}
