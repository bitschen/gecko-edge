package gecko;

/**
 * Interceptor 抛出此异常时，表示中断处理流程。
 *
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public final class DropException extends Exception {

    public DropException(String message) {
        super(message);
    }

}
