package gecko;

import gecko.lang.JSONObject;
import gecko.x.JsonX;

import java.util.Map;

/**
 * Outbound 作为Gecko的输出对象；
 *
 * @author Yoojia Chen (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public class Outbound {

    private final JSONObject mData;

    public Outbound() {
        mData = new JSONObject();
    }

    public Outbound addDataField(String name, Object value) {
        mData.field(name, value);
        return this;
    }

    public Outbound addDataFields(Map<String, Object> files) {
        mData.putAll(files);
        return this;
    }

    public JSONObject data() {
        return mData;
    }

    public byte[] toJSONBytes() {
        return JsonX.toJSONBytes(mData);
    }
}
