package gecko.frame;

public enum FrameType {
    BYTE,
    SHORT,
    INT,
    ARRAY
}