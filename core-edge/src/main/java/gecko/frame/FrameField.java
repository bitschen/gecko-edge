package gecko.frame;

public final class FrameField {

    public final String name;
    public final FrameType type;
    public final int byteSize;
    public final TypeConverter converter;

    public FrameField(String name, FrameType type, int byteSize, TypeConverter converter) {
        this.name = name;
        this.type = type;
        this.byteSize = byteSize;
        this.converter = converter;
    }

    public static FrameField BYTE(String name) {
        return BYTE(name, TypeConverter.TO_STRING);
    }

    public static FrameField BYTE(String name, TypeConverter converter) {
        return new FrameField(name, FrameType.BYTE, 1, converter);
    }

    public static FrameField SHORT(String name) {
        return SHORT(name, TypeConverter.TO_STRING);
    }

    public static FrameField SHORT(String name, TypeConverter converter) {
        return new FrameField(name, FrameType.SHORT, 2, converter);
    }

    public static FrameField INT(String name, TypeConverter converter) {
        return new FrameField(name, FrameType.INT, 4, converter);
    }

    public static FrameField INT(String name) {
        return INT(name, TypeConverter.TO_STRING);
    }

    public static FrameField ARRAY(String name, int length, TypeConverter converter) {
        return new FrameField(name, FrameType.ARRAY, length, converter);
    }

    public static FrameField ARRAY(String name, int length) {
        return ARRAY(name, length, TypeConverter.TO_STRING);
    }
}
