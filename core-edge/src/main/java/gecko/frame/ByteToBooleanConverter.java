package gecko.frame;

/**
 * Class description goes here.
 *
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public class ByteToBooleanConverter extends ByteToIntConverter {

    @Override
    protected Object convert0(int value) {
        return value != 0;
    }
}
