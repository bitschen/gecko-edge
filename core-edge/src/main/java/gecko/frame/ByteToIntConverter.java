package gecko.frame;

/**
 * 将Byte类型，转换成Int类型
 *
 * @author 陈哈哈 (yoojiachen@gmail.com, chenyongjia@parkingwang.com)
 * @version 0.1
 */
public class ByteToIntConverter implements TypeConverter {
    @Override
    public Object convert(FrameType type, Object value) {
        if (FrameType.BYTE.equals(type)) {
            return convert0((byte) value & 0xFF);
        } else {
            throw new IllegalArgumentException("输入类型不是Byte");
        }
    }

    protected Object convert0(int value) {
        return String.valueOf(value);
    }
}
