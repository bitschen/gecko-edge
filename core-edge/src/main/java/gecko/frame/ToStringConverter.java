package gecko.frame;

import gecko.x.ByteX;

/**
 * 转换成字符串类型
 *
 * @author 陈哈哈 (yoojiachen@gmail.com, chenyongjia@parkingwang.com)
 * @version 0.1
 */
public class ToStringConverter implements TypeConverter {

    @Override
    public Object convert(FrameType type, Object value) {
        switch (type) {
            case BYTE:
                return ByteX.hex((byte) value);
            case SHORT:
                return ByteX.hex((short) value);
            case INT:
                return String.valueOf(value);
            case ARRAY:
                return ByteX.hex((byte[]) value);
            default:
                return String.valueOf(value);
        }
    }
}