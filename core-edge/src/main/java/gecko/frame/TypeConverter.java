package gecko.frame;

/**
 * 类型转换接口；
 * 当 FrameAdapter 解析数据类型，并设置输出时，使用此接口来转换数据格式；
 *
 * @author 陈哈哈 (yoojiachen@gmail.com, chenyongjia@parkingwang.com)
 * @version 0.1
 */
public interface TypeConverter {

    /**
     * 根据类型，将数据转换成另一类型
     *
     * @param type  数据类型
     * @param value 数据
     * @return 另一数据类型
     */
    Object convert(FrameType type, Object value);

    /**
     * 默认将数据转换成字符串输出；
     */
    TypeConverter TO_STRING = new ToStringConverter();

}
