package gecko;


import gecko.lang.TypedMap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

/**
 * Driver抽象实现
 *
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.1
 */
public abstract class AbstractDriver implements Driver {

    private final Collection<Topic> mTopics = new ArrayList<>();
    private TypedMap mInitArgs = TypedMap.empty();
    private GeckoScoped mScoped;

    @Override
    public void onInit(TypedMap initArgs, GeckoScoped scoped) {
        mInitArgs = initArgs;
        mScoped = scoped;
    }

    @Override
    public void onStart(GeckoScoped scoped) {
        Objects.requireNonNull(mInitArgs, "onInit() not call");
        Objects.requireNonNull(mScoped, "onInit() not call");
    }

    @Override
    public void onStop(GeckoScoped scoped) {

    }

    public TypedMap getInitArgs() {
        return mInitArgs;
    }

    public GeckoScoped getScoped() {
        return mScoped;
    }

    @Override
    final public void setTopics(Collection<String> topics) {
        mTopics.clear();
        topics.forEach(t -> mTopics.add(Topic.create(t)));
    }

    @Override
    final public Collection<Topic> getTopics() {
        return mTopics;
    }


}
