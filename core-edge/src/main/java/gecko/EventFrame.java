package gecko;

import java.nio.ByteBuffer;

/**
 * Event事件载体，包含一个Header信息、数据体的数据实体。
 * 用于设备对象的请求和响应。
 *
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public class EventFrame {

    /**
     * 事件ID
     */
    public final long id;

    /**
     * 头部信息
     */
    public final Headers headers;
    /**
     * 数据帧
     */
    public final byte[] bytes;

    private EventFrame(long id, Headers headers, byte[] bytes) {
        this.id = id;
        this.headers = headers;
        this.bytes = bytes;
    }

    public EventFrame newOf(Headers headers, byte[] bytes) {
        return create(this.id, headers, bytes);
    }

    public EventFrame newOf(byte[] bytes) {
        return create(this.id, Headers.create(), bytes);
    }

    public ByteBuffer buffer() {
        return ByteBuffer.wrap(bytes);
    }

    //


    @Override
    public String toString() {
        return "EventFrame{" +
                "id=" + id +
                ", headers=" + headers +
                ", bytes=" + new String(bytes) +
                '}';
    }

    public static EventFrame create(long id, Headers headers, byte[] bytes) {
        return new EventFrame(id, headers, bytes);
    }

    public static EventFrame create(long id, byte[] bytes) {
        return new EventFrame(id, Headers.create(), bytes);
    }
}
