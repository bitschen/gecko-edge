package gecko;

/**
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public interface PipelineSelector {

    /**
     * 根据指定协议名，返回指定协议的Pipeline
     *
     * @param protocol 协议名
     * @return Pipeline
     */
    DevicePipeline find(String protocol);
}
