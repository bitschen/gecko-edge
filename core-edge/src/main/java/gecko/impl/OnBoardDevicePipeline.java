package gecko.impl;

import gecko.DevicePipeline;
import gecko.GeckoScoped;
import gecko.lang.TypedMap;

/**
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public class OnBoardDevicePipeline extends DevicePipeline {

    @Override
    public void onInit(TypedMap initArgs, GeckoScoped scoped) {

    }

    @Override
    public String getSupportProtocol() {
        return "onboard";
    }
}
