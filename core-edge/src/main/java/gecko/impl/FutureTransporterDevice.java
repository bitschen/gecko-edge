package gecko.impl;

import gecko.AbstractDevice;
import gecko.GeckoScoped;
import gecko.x.Strings;

/**
 * 由 {@link gecko.VirtualDevice.FutureTransporter} 实现异步RPC通讯；
 *
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public abstract class FutureTransporterDevice extends AbstractDevice {

    @Override
    public void onStart(GeckoScoped scoped) {
        super.onStart(scoped);
        getCheckedFutureTransporter();
        Strings.requireNotEmpty(getProtocolName(), "ProtocolName is required");
    }
}
