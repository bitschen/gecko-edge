package gecko.impl;

import gecko.DevicePipeline;
import gecko.GeckoScoped;
import gecko.lang.TypedMap;

/**
 * UDP通讯设备管道。
 * UDP设备对象作为客户端，内部自己实现了UDP的通讯功能；
 * 在Pipeline中，不需要其它操作。
 *
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public class UdpDevicePipeline extends DevicePipeline {

    @Override
    public void onInit(TypedMap initArgs, GeckoScoped scoped) {

    }

    @Override
    public String getSupportProtocol() {
        return UdpVirtualDevice.PROTOCOL;
    }
}
