package gecko;

/**
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public interface Interceptor extends InitAware, TopicFilter {

    /**
     * @return 优先级
     */
    int getPriority();

    /**
     * 设置优先级。
     */
    void setPriority(int priority);

    /**
     * 拦截处理过程。抛出 {@link DropException} 来中断拦截。
     *
     * @param ctx    GeckoContext
     * @param scoped GeckoScoped
     * @throws Exception 异常
     */
    void handle(GeckoContext ctx, GeckoScoped scoped) throws Exception;

    default void drop(String message) throws DropException {
        throw new DropException(message);
    }
}
