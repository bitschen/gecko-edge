package gecko;

import gecko.lang.JSONObject;

import java.net.SocketAddress;
import java.util.Collection;

/**
 * VirtualTrigger是一个负责接收前端事件，并调用 {@link ContextInvoker} 方法函数来向系统内部发起触发事件通知；
 * 内部系统处理完成后，将回调完成函数，返回输出
 *
 * @author 陈哈哈 (yoojiachen@gmail.com, chenyongjia@parkingwang.com)
 * @version 0.1
 */
public interface VirtualTrigger extends InitAware {

    /**
     * 启动
     */
    void onStart(GeckoScoped scoped, ContextInvoker invoker);

    /**
     * 停止
     */
    void onStop(GeckoScoped scoped, ContextInvoker invoker);

    ////

    /**
     * 用来发起请求，并输出结果
     */
    interface ContextInvoker {
        void invoke(Event event, Callback callback);
    }

    /**
     * 处理结果回调接口
     */
    interface Callback {
        void onComplete(String topic, JSONObject data);
    }

    /**
     * Trigger的协议数据适配器，用于解码编码数据包
     */
    interface Adapter {

        /**
         * 从输入端接收到数据包后，将数据包解码成JSONObject结构。
         * 单个数据包，可以返回一组事件。
         *
         * @param address 数据来源地址
         * @param bytes   字节数据
         * @return 事件组
         */
        Collection<Event> decode(SocketAddress address, byte[] bytes);

        /**
         * 内部系统处理完成后，返回JSONObject数据；编码接口负责转换成Trigger的数据格式；
         *
         * @param address 数据来源地址
         * @param data    JSONObject
         * @return 字节数组
         */
        byte[] encode(SocketAddress address, JSONObject data);
    }

    /**
     * 触发器事件
     */
    class Event {
        public final String topic;
        public final JSONObject data;

        public Event(String topic, JSONObject data) {
            this.topic = topic;
            this.data = data;
        }
    }
}
