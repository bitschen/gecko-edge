package gecko;


/**
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public abstract class AbstractHardware implements VirtualHardware {

    private String mName;
    private String mGroupAddress;
    private String mPhysicalAddress;

    @Override
    public void setDisplayName(String name) {
        this.mName = name;
    }

    @Override
    public String getDisplayName() {
        return this.mName;
    }

    @Override
    public void setGroupAddress(String address) {
        this.mGroupAddress = address;
    }

    @Override
    public String getGroupAddress() {
        return mGroupAddress;
    }

    @Override
    public void setPhysicalAddress(String address) {
        this.mPhysicalAddress = address;
    }

    @Override
    public String getPhysicalAddress() {
        return mPhysicalAddress;
    }

}
