package gecko;

import java.util.Collection;

/**
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public interface TopicFilter {

    /**
     * 设置事件Topic
     *
     * @param topics Topic
     */
    void setTopics(Collection<String> topics);

    /**
     * 获取事件Topic
     *
     * @return Topic
     */
    Collection<Topic> getTopics();
}
