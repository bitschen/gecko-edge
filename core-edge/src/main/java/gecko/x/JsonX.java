package gecko.x;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.Reader;
import java.lang.reflect.Type;

/**
 * JSON序列化工具类
 *
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public class JsonX {

    private final static Gson GSON = new GsonBuilder()
            .disableHtmlEscaping()
            .create();

    private JsonX() {
    }

    public static String toJSONString(Object model) {
        return GSON.toJson(model, model.getClass());
    }

    public static byte[] toJSONBytes(Object model) {
        return toJSONString(model).getBytes();
    }

    public static <T> T fromJSON(String json, Type type) {
        return GSON.fromJson(json, type);
    }

    public static <T> T fromJSON(Reader reader, Type type) {
        return GSON.fromJson(reader, type);
    }
}
