package gecko.x;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author 陈永佳 (yoojiachen@gmail.com)
 */
public class CollectionX {

    public static <T> Set<T> setOf(T... items) {
        Set<T> set = new HashSet<>(items.length);
        set.addAll(Arrays.asList(items));
        return set;
    }
}
