package gecko.x;

/**
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public class Strings {

    public static boolean isNullOrEmpty(String target) {
        return target == null || target.isEmpty();
    }

    public static String requireNotEmpty(String value) {
        return requireNotEmpty(value, "value is null or empty");
    }

    public static String requireNotEmpty(String value, String message) {
        if (isNullOrEmpty(value)) {
            throw new NullPointerException(message);
        }
        return value;
    }

    public static String safe(String s) {
        return s == null ? "" : s;
    }

}
