package gecko.x;

/**
 * @author 陈永佳 (yoojiachen@gmail.com)
 */
public class ClassX {

    public static final ClassLoader CLASS_LOADER;

    static {
        final ClassLoader cl = ClassLoader.getSystemClassLoader();
        if (cl == null) {
            CLASS_LOADER = ClassX.class.getClassLoader();
        } else {
            CLASS_LOADER = cl;
        }
    }

    private ClassX() {
    }

    @SuppressWarnings("unchecked")
    public static <T> T newObject(String className) {
        Class<?> clazz;
        try {
            clazz = CLASS_LOADER.loadClass(className);
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException(className, ex);
        }
        try {
            return (T) clazz.newInstance();
        } catch (Exception ex) {
            throw new RuntimeException(className, ex);
        }
    }

    /**
     * @param obj Object
     * @return 返回对象的类名，全路径
     */
    public static String classNameOf(Object obj) {
        return obj.getClass().getName();
    }

    /**
     * @param obj Object
     * @return 返回对象的类名，简称
     */
    public static String nameOf(Object obj) {
        return obj.getClass().getSimpleName();
    }
}
