package gecko;


import gecko.lang.TypedMap;

import java.util.Collection;

/**
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
class ConfigEntity {

    final String displayName;
    final boolean disable;
    final int priority;
    final String className;
    final String groupAddress;
    final String physicalAddress;
    final TypedMap initArgs;
    final Collection<String> topics;

    ConfigEntity(String displayName, String topic, boolean disable, int priority,
                 String className, String groupAddress, String physicalAddress,
                 TypedMap initArgs, Collection<String> topics) {
        this.displayName = displayName;
        this.disable = disable;
        this.priority = priority;
        this.className = className;
        this.groupAddress = groupAddress;
        this.physicalAddress = physicalAddress;
        this.initArgs = initArgs;
        this.topics = topics;
    }

    static ConfigEntity parse(String name, TypedMap m) {
        return new ConfigEntity(
                name,
                m.getString("topic"),
                m.getBoolean("disable"),
                m.getInt("priority"),
                m.getString("class"),
                m.getString("groupAddress"),
                m.getString("physicalAddress"),
                m.getDictMap("initArgs"),
                m.getStringList("topics"));
    }
}
