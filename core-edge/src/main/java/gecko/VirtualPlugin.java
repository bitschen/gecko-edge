package gecko;

/**
 * VirtualPlugin 用于隔离其它类型与 Bundle 的类型。
 *
 * @author 陈哈哈 (yoojiachen@gmail.com, chenyongjia@parkingwang.com)
 * @version 0.1
 */
public interface VirtualPlugin extends Bundle {

}
