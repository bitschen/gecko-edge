package gecko.lang;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public class AwaitValue<T> {

    private T mValue;
    private final CountDownLatch mLatch = new CountDownLatch(1);

    public T getAwait() throws InterruptedException {
        mLatch.await();
        return mValue;
    }

    public T getAwait(long timeout, TimeUnit unit) throws InterruptedException {
        mLatch.await(timeout, unit);
        return mValue;
    }

    public void set(T value) {
        if (mLatch.getCount() > 0) {
            mLatch.countDown();
            mValue = value;
        }
    }
}
