package gecko.lang;

/**
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public class ShutdownHook {

    private ShutdownHook() {
    }

    public static void add(Runnable task) {
        Runtime.getRuntime().addShutdownHook(new Thread(task));
    }
}
