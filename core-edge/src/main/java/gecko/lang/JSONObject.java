package gecko.lang;

import gecko.x.JsonX;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public class JSONObject extends LinkedHashMap<String, Object> {

    public JSONObject(int i) {
        super(i);
    }

    public JSONObject() {
    }

    public JSONObject(Map<? extends String, ?> map) {
        super(map);
    }

    public JSONObject field(String key, Object value) {
        put(key, value);
        return this;
    }

    public JSONObject fields(Map<String, Object> fields) {
        putAll(fields);
        return this;
    }

    public byte[] toJSONBytes() {
        return JsonX.toJSONBytes(this);
    }

    public String toJSONString() {
        return JsonX.toJSONString(this);
    }

    ////

    public static JSONObject parse(String json) {
        return JsonX.fromJSON(json, JSONObject.class);
    }

    public static JSONObject parse(InputStream is) {
        return JsonX.fromJSON(new InputStreamReader(is), JSONObject.class);
    }

    /**
     * 指定Key Value 构建一个JSON对象
     *
     * @param key   Name
     * @param value Value
     * @return JSONObject
     */
    public static JSONObject of(String key, Object value) {
        final JSONObject json = new JSONObject();
        json.put(key, value);
        return json;
    }

    public static JSONObject error(String message) {
        return of("error", message);
    }
}
