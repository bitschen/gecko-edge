package gecko.lang;

import java.io.*;

/**
 * @author 陈哈哈 (yoojiachen@gmail.com, chenyongjia@parkingwang.com)
 * @version 0.1
 */
public class StringReader {

    public static long copyTo(Reader reader, Writer writer, int bufferSize) throws IOException {
        int size = 0;
        char[] buffer = new char[bufferSize];
        int read = reader.read(buffer);
        while (read >= 0) {
            writer.write(buffer, 0, read);
            size += read;
            read = reader.read(buffer);
        }
        return size;
    }

    public static long copyTo(Reader reader, Writer writer) throws IOException {
        return copyTo(reader, writer, 4 * 1024);
    }

    public static String read(InputStreamReader reader) throws IOException {
        final StringWriter sw = new StringWriter();
        copyTo(reader, sw);
        return sw.toString();
    }

    public static String read(InputStream is) throws IOException {
        return read(new InputStreamReader(is));
    }
}
