package gecko;

import gecko.lang.JSONObject;

import java.util.Collections;

/**
 * Inbound 作为Gecko输入数据模型。
 *
 * @author Yoojia Chen (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public class Inbound {

    private final JSONObject mData;

    Inbound(JSONObject data) {
        mData = data;
    }

    /**
     * 读取数据字段，返回的Map对象是不可变对象。
     *
     * @return 不可变JSONObject
     */
    public JSONObject data() {
        return new JSONObject(Collections.unmodifiableMap(mData));
    }

}
