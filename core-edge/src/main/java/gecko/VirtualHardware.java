package gecko;

/**
 * VirtualHardware 是一个硬件设备的表示符号。
 * * 每个设备对象，至少包含以下信息：
 * 1. 通讯协议： inapp, rs485, tcp, zmq, mqtt
 * 2. 设备名称，对当前设备的命名；
 * 3. 设备属组地址，表示设备归属某一组。它与 PhysicalAddress 共同决定一个设备的坐标；
 * 4. 设备物理地址。
 *
 * @author 陈永佳 (yoojiachen@gmail.com)
 */
interface VirtualHardware {

    /**
     * 设置设备名称
     *
     * @param name 设备名称
     */
    void setDisplayName(String name);

    /**
     * 获取设备名称
     *
     * @return 设备名称
     */
    String getDisplayName();

    /**
     * 设置设备属组地址
     *
     * @param address 地址
     */
    void setGroupAddress(String address);

    /**
     * @return 返回设备属组地址
     */
    String getGroupAddress();

    /**
     * 设置设备物理地址
     *
     * @param address 地址
     */
    void setPhysicalAddress(String address);

    /**
     * @return 返回设备地址
     */
    String getPhysicalAddress();

    /**
     * 获取设备地址，由 /{GroupAddress}/{PhysicalAddress} 组成。
     *
     * @return Address
     */
    default String getUnionAddress() {
        return "/" + getGroupAddress() + "/" + getPhysicalAddress();
    }

    /**
     * @return 返回当前设备支持的通讯协议名称
     */
    String getProtocolName();

    /**
     * 返回当前硬件的地址对象
     *
     * @param domain Domain
     * @param nodeId NodeId
     * @return Address
     */
    default Address getAddress(String domain, String nodeId) {
        return Address.create(getProtocolName(), domain, nodeId, getGroupAddress(), getPhysicalAddress());
    }
}
