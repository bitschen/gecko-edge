package gecko;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 陈永佳 (yoojiachen@gmail.com)
 */
public class Headers extends HashMap<String, String> {

    public Headers addHeader(String name, String value) {
        put(name, value);
        return this;
    }

    public Headers addHeaders(Map<String, String> headers) {
        putAll(headers);
        return this;
    }

    public static Headers create() {
        return new Headers();
    }

}
