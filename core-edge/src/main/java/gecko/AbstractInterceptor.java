package gecko;


import gecko.lang.TypedMap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

/**
 * Interceptor抽象实现
 *
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.1
 */
public abstract class AbstractInterceptor implements Interceptor {

    private final Collection<Topic> mTopics = new ArrayList<>();

    private int mPriority = 0;
    private TypedMap mInitArgs = TypedMap.empty();
    private GeckoScoped mScoped;

    @Override
    public void onInit(TypedMap initArgs, GeckoScoped scoped) {
        mInitArgs = initArgs;
        mScoped = scoped;
    }

    @Override
    public int getPriority() {
        return mPriority;
    }

    @Override
    public void setPriority(int priority) {
        mPriority = priority;
    }

    public TypedMap getInitArgs() {
        return Objects.requireNonNull(mInitArgs, "onInit() not call");
    }

    public GeckoScoped getScoped() {
        return Objects.requireNonNull(mScoped, "onInit() not call");
    }

    @Override
    final public void setTopics(Collection<String> topics) {
        mTopics.clear();
        topics.forEach(t -> mTopics.add(Topic.create(t)));
    }

    @Override
    final public Collection<Topic> getTopics() {
        return mTopics;
    }


}
