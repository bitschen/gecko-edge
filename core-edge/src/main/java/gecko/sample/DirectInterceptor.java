package gecko.sample;

import gecko.AbstractInterceptor;
import gecko.GeckoContext;
import gecko.GeckoScoped;

/**
 * 不做任何检查的拦截器
 *
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public class DirectInterceptor extends AbstractInterceptor {

    @Override
    public void handle(GeckoContext context, GeckoScoped scoped) throws Exception {

    }

}
