package gecko;

import gecko.lang.TypedMap;

/**
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public interface InitAware {

    /**
     * 初始化
     *
     * @param initArgs 初始化参数
     */
    void onInit(TypedMap initArgs, GeckoScoped scoped);

}