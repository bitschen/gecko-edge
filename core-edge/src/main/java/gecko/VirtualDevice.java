package gecko;

import java.util.concurrent.CompletableFuture;

/**
 * Device是一个硬件设备的表示符号。
 * 它可以接收由其它组件派发到此设备的事件，做出操作后，返回一个响应事件。
 *
 * @author 陈永佳 (yoojiachen@gmail.com)
 * @version 0.0.1
 */
public interface VirtualDevice extends VirtualHardware, Bundle {

    /**
     * 设备对象接收控制事件；经设备驱动处理后，返回处理结果事件；
     *
     * @param event  输入事件
     * @param scoped GeckoScoped
     * @return 结果事件
     * @throws Exception 设备发生错误时抛出异常
     */
    CompletableFuture<EventFrame> process(EventFrame event, GeckoScoped scoped) throws Exception;

    /**
     * FutureTransporter 是设备异步处理事件的接口；设备使用此接口来实现异步处理事件；
     */
    interface FutureTransporter {

        CompletableFuture<EventFrame> transport(EventFrame event, GeckoScoped scoped, Address address);
    }
}
